public static final String INPUT_FILE_NAME = "/tmp/inputFileName.jpg"; 
PImage img;
ArrayList<PVector> points;
PVector q;
int n;
float factor;
void setup () {
  size (600, 600);
  colorMode(HSB);
  background (0);
  points = new ArrayList<PVector>();
  img = loadImage(INPUT_FILE_NAME);
  if (img.width > img.height)
    img.resize(600, 600);
  else
    img.resize(600, 600);
  /*img.filter(ERODE);img.filter(ERODE);
  img.filter(ERODE);img.filter(ERODE);
  img.filter(ERODE);img.filter(ERODE);
  img.filter(ERODE);img.filter(ERODE);
  img.filter(ERODE);img.filter(ERODE);
  img.filter(ERODE);img.filter(ERODE);*/
  n = 6;
  factor = (float)1/2;
  float radius = 300;
  
  q = new PVector (random(width), random(height));
  for (int i = 0; i < n; i++) {
    float theta = i*TWO_PI/n;
    PVector p = new PVector (radius*cos(theta), radius*sin(theta));
    p.add(width/2, height/2);
    points.add(p);
  }
  stroke(255);
  fill(255);
  for (PVector p : points) {
    point (p.x, p.y);
  }
  //image(img,0,0);
}
int prev = -1;
int prevprev = -2;
int velocity = 440;
void draw () {
  int r;
  PVector p;
  //if (frameCount %1000 == 0) reset();
  for (int i = 0; i < velocity; i++) {
    do {
      r = floor(random(n));
      p = points.get(r);
    }while (/*prevprev == prev && */(r == (prev+1)%n || r == (prev-1)%n));
    //while (r == prev);
    //while (blue(img.get(round((q.x+p.x)*factor*img.width/600), round((q.y+p.y)*factor*img.height/600))) < 100);
    prevprev = prev;
    prev = r;
    
    q.x = ((q.x+p.x)*factor);
    q.y = ((q.y+p.y)*factor);
    float dist = q.dist(new PVector(width/2, height/2));
    //stroke(255*dist/( height/2 ), 50, 100);
    
    color c = img.get(round(q.x+100), round(q.y-50));
    //color c = (255);
    //color c = color(random(255), random(255), random(255));
    stroke (c);
    point (q.x, q.y);
  }
}

void reset() {
  background (0);
  points = new ArrayList<PVector>();
  n++;
  factor = (float)1/2;
  float radius = 300;
  
  q = new PVector (random(width), random(height));
  for (int i = 0; i < n; i++) {
    float theta = i*TWO_PI/n;
    PVector p = new PVector (radius*cos(theta), radius*sin(theta));
    p.add(width/2, height/2);
    points.add(p);
  }
  stroke(255);
  fill(255);
  for (PVector p : points) {
    point (p.x, p.y);
  }
}
